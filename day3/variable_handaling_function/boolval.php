




<?php
echo '0:        '.(boolval(0) ? 'true' : 'false')."\n";
echo"<br>";

//evulate to true because $var is empty
if (empty($var))
{
    echo '$var is either 0, empty, or not set at all';
}
//evulate as true because $var is set
if(isset($var)){
    echo '$var is set even though it is empty';
}

echo "<br>";
$data = array(1, 1., NULL, new stdClass, 'foo');

foreach ($data as $value) {
    echo gettype($value), "\n";
}
echo "<br>";

$yes = array('this', 'is', 'an array');
echo is_array($yes) ? 'Array' : 'not an array';
echo "\n";
$no ='this is a string';
echo is_array($no) ? 'Array' : 'not an array';
echo "<br>";

$n1=100;
$n2=.1000.;
$n3=1000.0;
echo gettype($n1);
echo "<br>";
echo gettype($n2);
echo "<br>";
echo gettype($n3);
?>