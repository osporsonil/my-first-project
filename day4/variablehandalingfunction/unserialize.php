<?php

$ar = array('a' , 'b' , 'c');
echo "<pre>";
print_r($ar);
echo gettype($ar). "<br>";

$after_serialize = serialize($ar);

echo gettype($after_serialize). "<br>";
echo $after_serialize. "<br>";

$previous_serialize = unserialize($after_serialize);
echo gettype($previous_serialize). "<br>";

print_r($previous_serialize);